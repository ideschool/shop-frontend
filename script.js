const Api = function () {
    this.url = 'https://d3f70ca1.ngrok.io/db/sklepik';
};

Api.prototype.post = function (path, data) {
    const url = this.url + '/' + path;
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => response.json());
};

Api.prototype.put = function (path, data) {
    const url = this.url + '/' + path;
    return fetch(url, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => response.json());
};

Api.prototype.get = function (path) {
    const url = this.url + '/' + path;
    return fetch(url);
};

Api.prototype.delete = function (path) {
    const url = this.url + '/' + path;
    return fetch(url, {method: 'DELETE'});
};

Api.prototype.getAll = function () {
    return this.get('');
};

Api.prototype.getProduct = function (id) {
    return this.get(id);
};

Api.prototype.addProduct = function (id, data) {
    return this.post(id, data);
};

Api.prototype.updateProduct = function (id, data) {
    return this.put(id, data);
};

Api.prototype.deleteProduct = function () {
    return
};

Api.prototype.buyProduct = function () {
};

const api = new Api();
api.post('1aaa', {name: 'foo', count: 10, price: 9.99}).then(resp => {
    console.log(resp);
});
